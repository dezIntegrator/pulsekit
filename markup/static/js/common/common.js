(function () {
    // плавный скролл
    $('a[href^="#section"]').click(function () {
        var _href = $(this).attr('href');
        $('html, body').animate({scrollTop: $(_href).offset().top + 'px' });
        return false;
    });
    // /плавный скролл

    // хелперы для модального окна
    var scrollbarWidth = false;

    function getScrollbarWidth() {
        if (scrollbarWidth !== false) {
            return scrollbarWidth;
        }
        /* istanbul ignore else */
        if (typeof document !== 'undefined') {
            var div = document.createElement('div');
            div.style.width = '100px';
            div.style.height = '100px';
            div.style.position = 'absolute';
            div.style.top = '-9999px';
            div.style.overflow = 'scroll';
            div.style.MsOverflowStyle = 'scrollbar';
            document.body.appendChild(div);
            scrollbarWidth = div.offsetWidth - div.clientWidth;
            document.body.removeChild(div);
        } else {
            scrollbarWidth = 0;
        }
        return scrollbarWidth || 0;
    }

    window.getScrollbarWidth = getScrollbarWidth;

    var $body = $('body');

    function getBody(update) {
        if (update) {
            $body = $('body');
        }
        return $body;
    }

    window.getBody = getBody;
    // /хелперы для модального окна

    function emailValidator(email) {
        if (!email) {
            return false;
        }
        var regExpMail = /^[^\@]+@.*\.[a-zа-я]{2,6}$/g;
        return email.toLowerCase().match(regExpMail);
    }

    window.emailValidator = emailValidator;
})();
