let data= { 
    overview: {
        timesheet: {
            imageModalPath: "%=static=%img/assets/modalOverview/TimesheetOverview.png"
        },
        checklist: {
            imageModalPath: "%=static=%img/assets/modalOverview/ChecklistOverview.png"
        },
        badges: {
            imageModalPath: "%=static=%img/assets/modalOverview/BadgesOverview.png" 
        },
        project: {
            imageModalPath: "%=static=%img/assets/modalOverview/ProjectOverview.png"
        }
    }
}