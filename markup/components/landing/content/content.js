(function () {

    // проверяет на наличие блока с контентом
    var $landContent = $('.s-land-content');
    if ($landContent.length < 1) {
        return;
    }

    function switchItem() {
        // меняет активный блок на нажатый элемент
        var $landLeftSideItemActive = $('.land-left-side__item_active');
        var $landContentIconActive = $('.land-content__icon_active');
        $landLeftSideItemActive.removeClass('land-left-side__item_active');
        $landContentIconActive.removeClass('land-content__icon_active');
        $(this).addClass('land-left-side__item_active');
        $('.land-left-side__item_active > .land-content__icon').addClass('land-content__icon_active');
        // смена активного слайда
        var landContentRightSideItemIndex = $(this).index(); // индекс нажатого элемента
        var $landContentRightSideItemActive = $('.land-right-side__item_active');
        $landContentRightSideItemActive.removeClass('land-right-side__item_active');

        var $landContentRightSideItem = $('.land-right-side__item').eq(landContentRightSideItemIndex);
        $landContentRightSideItem.addClass('land-right-side__item_active');
        // сдвиг скролла
        var $landContentBorderLine = $('.land-content__border_line');
        $landContentBorderLine.css('margin-top', '' + 54 * landContentRightSideItemIndex + 'px');
    }
    // привязка функции к элементу списка
    var $landLeftSideItem = $('.land-left-side__item');
    $landLeftSideItem.on('click', switchItem);

    $('.land-content__wrapper_mobile-slider').slick({
        dots: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
        variableWidth: true,
        centerMode: true,
        initialSlide: 0,
        centerPadding: 0,
        swipe: true,
        prevArrow: '<a class="slick-prev arrow arrow-right arrow-right_land-content">',
        nextArrow: '<a class="slick-next arrow arrow-left arrow-left_land-content">'
    });
})();
