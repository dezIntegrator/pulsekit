let data= { 
    footer: {
        timesheet: {
            backroundColorClass: 's-land-footer__backgound_timesheet',
            emailColorClass: 's-land-footer__copyright-mail-address-color_timesheet',
            followColorClass: 's-land-footer__content-link-updates-color_timesheet'
        },
        checklist: {
            backroundColorClass: 's-land-footer__backgound_checklist',
            emailColorClass: 's-land-footer__copyright-mail-address-color_checklist',
            followColorClass: 's-land-footer__content-link-updates-color_checklist'
        },
        badges: {
            backroundColorClass: 's-land-footer__backgound_badges',
            emailColorClass: 's-land-footer__copyright-mail-address-color_badges',
            followColorClass: 's-land-footer__content-link-updates-color_badges'
        },
        project: {
            backroundColorClass: 's-land-footer__backgound_project',
            emailColorClass: 's-land-footer__copyright-mail-address-color_project',
            followColorClass: 's-land-footer__content-link-updates-color_project'
        }
    }
}