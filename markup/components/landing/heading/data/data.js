let data= { 
    heading: {
        timesheet: {
            title: 'Pulse.Timesheet', 
            subtitle: 'Pulse.Timesheet', 
            headingBackgroundClass: 's-land-heading_timesheet',
        },
        checklist: {
            title: 'Pulse.Checklist',
            subtitle: 'Create ideal acceptance criteria or any ToDo lists using checklists.',
            headingBackgroundClass: 's-land-heading_checklist',
        },
        badges: {
            title: 'Pulse.Badges', 
            subtitle: "Pulse.Checklist", 
            headingBackgroundClass: 's-land-heading_badges',
        },
        project: {
            title: 'Pulse.Project',
            subtitle: 'Pulse.Checklist',
            headingBackgroundClass: 's-land-heading_project',
        }
    }
}