(function () {

    var $versionItemChangelog = $('.land-heading__version_item-changelog');
    var $versionItemVersion = $('.land-heading__version_item-version');
    var $versionItemRoadmap = $('.land-heading__version_item-roadmap');

    if ($versionItemChangelog.length < 1 || $versionItemVersion.length < 1 || $versionItemRoadmap.length < 1) {
        return;
    }

    $versionItemChangelog.on('mouseover', function () {
        $versionItemVersion.addClass('land-heading__version_item-version_inactive');
    });

    $versionItemChangelog.on('mouseout', function () {
        $versionItemVersion.removeClass('land-heading__version_item-version_inactive');
    });


    $versionItemRoadmap.on('mouseover', function () {
        $versionItemVersion.addClass('land-heading__version_item-version_inactive');
    });

    $versionItemRoadmap.on('mouseout', function () {
        $versionItemVersion.removeClass('land-heading__version_item-version_inactive');
    });

})();
