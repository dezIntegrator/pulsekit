let data= { 
    suggestions: {
        timesheet: {
            backgroundColorClass: 's-land-suggestions-background_timesheet',
            buttonColorClass: 'btn-color_timesheet'
        },
        checklist: {
            backgroundColorClass: 's-land-suggestions-background_checklist',
            buttonColorClass: 'btn-color_checklist'
        },
        badges: {
            backgroundColorClass: 's-land-suggestions-background_badges',
            buttonColorClass: 'btn-color_badges'
        },
        project: {
            backgroundColorClass: 's-land-suggestions-background_project',
            buttonColorClass: 'btn-color_project'
        }
    }
}