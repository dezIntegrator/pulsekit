(function () {

    $(window).on('load', function () {
        if ($('.s-land-suggestions').length === 0) {
            return;
        }

        var commentsVm = new Vue({
            el: '.s-land-suggestions',
            data: {
                comment: ''
            },
            methods: {
                sendComment: function () {
                    // `this` внутри методов указывает на экземпляр Vue
                    formCommentsEmail.openForm(this.comment, this.clearComment.bind(this));
                },
                clearComment: function () {
                    this.comment = '';
                }
            }
        });
    });

})();
