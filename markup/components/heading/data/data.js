var data = {
    heading: {
        timesheet_roadmap: {
            title: "Roadmap", 
            subtitle: "Pulse.Timesheet", 
            headingBackgroundClass: 's-heading_timesheet',
            link2Landing: 'index.html'
        },
        timesheet_changelog: {
            title: 'Changelog',
            subtitle: 'Pulse.Timesheet',
            headingBackgroundClass: 's-heading_timesheet',
            link2Landing: 'index.html'
        },
        checklist_roadmap: {
            title: "Roadmap", 
            subtitle: "Pulse.Checklist", 
            headingBackgroundClass: 's-heading_checklist',
            link2Landing: 'index.html'
        },
        checklist_changelog: {
            title: 'Changelog',
            subtitle: 'Pulse.Checklist',
            headingBackgroundClass: 's-heading_checklist',
            link2Landing: 'index.html'
        },
        privacy: {
            title: 'Data Security and Privacy Statement',
            subtitle: 'February 1, 2017',
            headingBackgroundClass: '',
            link2Landing: 'index.html'
            
        },
        license: {
            title: 'End User License Agreement',
            subtitle: 'February 1, 2017',
            headingBackgroundClass: '',
            link2Landing: 'index.html'            
        }
    }
}