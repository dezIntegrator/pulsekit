// работаем с формой обратной связи
(function () {
    $(window).on('load', function () {

        if ($('.js-form-comments-email').length === 0) {
            return;
        }

        var formCommentsEmailVm = new Vue({
            el: '.js-form-comments-email',
            data: {
                comment: '',
                email: ''
            },
            computed: {
                isEmailValid: function () {
                    return emailValidator(this.email);
                }
            }
        });

        var formCommentsEmail = {};

        var cbCloseSuccessSubmitForm = function () {};
        var cbCloseNoSubmitForm = function () {};

        var closeSuccessSubmitForm = function () {
            cbCloseSuccessSubmitForm();
        };
        var closeNoSubmitForm = function () {
            cbCloseNoSubmitForm();
        };

        // Открытие из других модулей
        formCommentsEmail.openForm = function (comment, _cbCloseSuccessSubmitForm, _cbCloseNoSubmitForm) {
            moduleModal.openModal($('#modal-comments-email'));
            formCommentsEmailVm.$data.comment = comment;
            cbCloseSuccessSubmitForm = function () {
                _cbCloseSuccessSubmitForm();
            };
            cbCloseNoSubmitForm = function () {
                _cbCloseNoSubmitForm();
            };
        };

        window.formCommentsEmail = formCommentsEmail;

        var $form = $('.js-form-comments-email');

        var URL_SUBMIT_FORM = '../sendmail/sendmail.php';

        modalForm.modalFormAddEvent($form, URL_SUBMIT_FORM, closeSuccessSubmitForm);
    });
})();
