// Работаем с формой подписки
(function() {
    $(window).on('load', function() {

        if (!$('.js-form-subscription').length) {
            return;
        }

        var formSubscriptionVm = new Vue ({
            el: '.js-form-subscription',
            data: {
                email: ''
            },
            computed: {
                isEmailValid: function () {
                    return emailValidator(this.email);
                }
            }
        });


        var $form = $('.js-form-subscription');

        var URL_SUBMIT_FORM = '../subscribe/subscribe.php';

        modalForm.modalFormAddEvent($form, URL_SUBMIT_FORM);

    });
})();
