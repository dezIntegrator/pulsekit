let data= { 
    modalFormSubscription: {
        timesheet: {
            buttonColorClass: 'btn-color_timesheet'
        },
        checklist: {
            buttonColorClass: 'btn-color_checklist'
        },
        badges: {
            buttonColorClass: 'btn-color_badges'
        },
        project: {
            buttonColorClass: 'btn-color_project'
        }
    }
}