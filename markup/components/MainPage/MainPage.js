(function () {

    var $mainPageMainBlock = $('.s-main-page-wrapper');
    if ($mainPageMainBlock.length < 1) {
        return;
    }

    function addClassesLogo(block, shadow) {
        block.add('s-main-page-pluginLogo__content__item-wrapper--active');
        shadow.add('s-main-page-pluginLogo__content__box-shadow--active');
    }

    function removeClassesLogo(block, shadow) {
        block.remove('s-main-page-pluginLogo__content__item-wrapper--active');
        shadow.remove('s-main-page-pluginLogo__content__box-shadow--active');
    }

    var time = document.getElementById('time'),
        checklist = document.getElementById('check'),
        badges = document.getElementById('badge'),
        projects = document.getElementById('projects'),
        timeClassList = document.getElementById('timeLogo').classList,
        timeShadowClassList = document.getElementById('timeLogoShadow').classList;

    time.addEventListener('mouseover', function () {
        addClassesLogo(timeClassList, timeShadowClassList);
    });
    time.addEventListener('mouseout', function () {
        removeClassesLogo(timeClassList, timeShadowClassList);
    });

    var checkClassList = document.getElementById('checkLogo').classList,
        checkShadowClassList = document.getElementById('checkLogoShadow').classList;
    checklist.addEventListener('mouseover', function () {
        addClassesLogo(checkClassList, checkShadowClassList);
    });
    checklist.addEventListener('mouseout', function () {
        removeClassesLogo(checkClassList, checkShadowClassList);
    });

    var badgesClassList = document.getElementById('badgesLogo').classList,
        badgesShadowClassList = document.getElementById('badgesLogoShadow').classList;
    badges.addEventListener('mouseover', function () {
        addClassesLogo(badgesClassList, badgesShadowClassList);
    });
    badges.addEventListener('mouseout', function () {
        removeClassesLogo(badgesClassList, badgesShadowClassList);
    });

    var projClassList = document.getElementById('projectsLogo').classList,
        projShadowClassList = document.getElementById('projectsLogoShadow').classList;
    projects.addEventListener('mouseover', function () {
        addClassesLogo(projClassList, projShadowClassList);
    });
    projects.addEventListener('mouseout', function () {
        removeClassesLogo(projClassList, projShadowClassList);
    });

    function onScrollWindowForAddClassName() {
        var offsetY = window.pageYOffset;
        var animationOffset = 250;

        var elements = document.getElementsByClassName('s-main-page__content__item--header-icon');

        if (elements.length < 1 || $(elements[3]).hasClass('s-main-page__content__item--header-icon_active')) {
            return;
        }

        if (((offsetY + screen.height) - $(elements[0]).offset().top) > animationOffset ) {
            elements[0].classList.add('s-main-page__content__item--header-icon_active');
        }
        if (((offsetY + screen.height) - $(elements[1]).offset().top) > animationOffset ) {
            elements[1].classList.add('s-main-page__content__item--header-icon_active');
        }
        if (((offsetY + screen.height) - $(elements[2]).offset().top) > animationOffset ) {
            elements[2].classList.add('s-main-page__content__item--header-icon_active');
        }
        if (((offsetY + screen.height) - $(elements[3]).offset().top) > animationOffset ) {
            elements[3].classList.add('s-main-page__content__item--header-icon_active');
        }
    }


    $(document).ready(function () {
        $('#startSlider').slick({
            arrows: true,
            dots: false,
            autoplay: true,
            autoplaySpeed: 3500,
            speed: 500,
            slidesToShow: 1,
            slidesToScroll: 1,
            variableWidth: true,
            draggable: true,
            centerMode: true,
            initialSlide: 1,
            infinite: true,
            centerPadding: 0,
            swipe: true,
            prevArrow: '<a class="slick-prev arrow arrow-left">',
            nextArrow: '<a class="slick-next arrow arrow-right">'
        });

        window.windowWidth = window.innerWidth;

        Math.easeInOutQuad = function (t, b, c, d) {
            t /= d / 2;
            if (t < 1) {
                return c / 2 * t * t + b;
            }
            t--;
            return -c / 2 * (t * (t - 2) - 1) + b;
        };

        document.addEventListener('scroll', function () {
            onScrollWindowForAddClassName();
        }, true);

        $('.s-main-page-pluginLogo__content__item').on('mouseover', function () {
            var item = $(this).find('.s-main-page-pluginLogo__content__item'),
                shadow = $(this).find('.s-main-page-pluginLogo__content__box-shadow'),
                itemId = item.data('val');
            $(document).find('#' + itemId).find('.s-main-page-pluginDesc__content__item').addClass('s-main-page-pluginDesc__content__item__onLogoEnter');
            item.addClass('s-main-page-pluginLogo__content__item-wrapper--active');
            shadow.addClass('s-main-page-pluginLogo__content__box-shadow--active');
        });

        $('.s-main-page-pluginLogo__content__item').on('mouseout', function () {
            var item = $(this).find('.s-main-page-pluginLogo__content__item'),
                shadow = $(this).find('.s-main-page-pluginLogo__content__box-shadow'),
                itemId = item.data('val');
            $(document).find('#' + itemId).find('.s-main-page-pluginDesc__content__item').removeClass('s-main-page-pluginDesc__content__item__onLogoEnter');
            item.removeClass('s-main-page-pluginLogo__content__item-wrapper--active');
            shadow.removeClass('s-main-page-pluginLogo__content__box-shadow--active');
        });

        $('.s-main-page-preorder__button-wrapper').on('mouseover', function () {
            $('.b-button__content-text').html('COMING SOON');
        });

        $('.s-main-page-preorder__button-wrapper').on('mouseout', function () {
            $('.b-button__content-text').html('ALL ADD-ONS');
        });
    });
})();
