var $sOverviewModal = $('.s-overview__modal');
var $bModalWrapper = $('.b-modal__wrapper');

$(document).on('ready', function () {
    $sOverviewModal.on('click', playVideo);
    $bModalWrapper.on('click', stopVideo);
});

var tag = document.createElement('script');

tag.src = "https://www.youtube.com/iframe_api";
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

var player;
var playerMobile;

function onYouTubeIframeAPIReady() {

    player = new YT.Player( 'player', {
        width: 1024,
        height: 596,
        videoId: '3gG7pdDepNk',
        playerVars: { 'rel': 0, 'showinfo': 0, 'frameborder': 0, 'allowfullscreen': true },
        events: {
            'onStateChange': onPlayerStateChange
        }
    });

    playerMobile = new YT.Player( 'playerMobile', {
        width: '100%',
        height: '100%',
        videoId: '3gG7pdDepNk',
        playerVars: { 'rel': 0, 'showinfo': 0, 'frameborder': 0, 'allowfullscreen': true },
    });
}

function onPlayerStateChange(event) {
    if (event.data == YT.PlayerState.ENDED) {
        closeModal();
    }
}

function closeModal() {
    moduleModal.closeModal($('#modal-overview'));
}

function playVideo() {
    player.playVideo();
}

function stopVideo() {
    player.stopVideo();
}

// Работа с модальным окном

var moduleModal = {
    openModal: function ($elModal) {
        getBody().css({'overflow': 'hidden', 'padding-right': getScrollbarWidth()});
        $elModal.css({
            'display': 'block'
        });

        $elModal.on('click', function (event) {
            var $target = $(event.target);
            var isClickCloseBtn = $target === $elModal.find('.js-modal__close') || $target.closest('.js-modal__close').length;

            if (!isClickCloseBtn && $target.closest('.js-modal__content').length) {
                return;
            } else {
                moduleModal.closeModal($elModal);
            }
        });
    },

    closeModal: function ($elModal) {
        getBody().css({'overflow': '', 'padding-right': ''});

        $elModal.css({'display': ''});
    }
};

(function() {
    $(document).ready(function() {
        $('.js-open-modal').each(function (index, item) {
            var $item = $(item);
            $item.on('click', moduleModal.openModal.bind(null, $($item.data('modal'))));
        });
    });
})();

