var data = {
    content: {
        checklist_roadmap: {
            text: 
        `
        <h2 class="content__title">Release: june-july 2017</h2>      <p class="content__text">- The ability to add additional fields to the report</p> <h2 class="content__title">Release: summer 2017</h2>        <p class="content__text">- New filters appear on the "Reports" page.</p>        <h2 class="content__title">Release: autumn 2017</h2>        <p class="content__text">- New window "Log Work" with additional parameters.</p>  <h2 class="content__title">Release: autumn-winter 2017</h2>  <p class="content__text">- Viewing the report before downloading.</p>
        `
        },
        checklist_changelog: {
            text: 
        `
        <h2 class="content__title">15 May 2017</h2>      
        <p class="content__text">- Now you can save the selected report parameters as a template.</p> 
        <p class="content__text">- Now you can configure the sending of reports to your mail on a schedule.</p> 
        <p class="content__text">- Fixed the problem with the lack of an Epic link from the subtask.</p> 
        <p class="content__text">- Now you can send a report to your e-mail.</p> 
        <h2 class="content__title">24 March 2017</h2>        
        <p class="content__text">- Optimization of report unloading, which made it possible to generate a report 10 times faster.</p>        
        <h2 class="content__title">20 March 2017</h2>        
        <p class="content__text">- Fixed the problem with displaying an incomplete list of Executors with some rights settings.</p>  
        <h2 class="content__title">17 March 2017</h2>  
        <p class="content__text">- Fixed the problem with incorrect generation of report resolution in Safari.</p>
        <h2 class="content__title">1 March 2017</h2>  
        <p class="content__text">- Published add-on in the production.</p>
        `
        },
        timesheet_roadmap: {
            text: 
        `
        <h2 class="content__title">Release: june-july 2017</h2>      <p class="content__text">- The ability to add additional fields to the report</p> <h2 class="content__title">Release: summer 2017</h2>        <p class="content__text">- New filters appear on the "Reports" page.</p>        <h2 class="content__title">Release: autumn 2017</h2>        <p class="content__text">- New window "Log Work" with additional parameters.</p>  <h2 class="content__title">Release: autumn-winter 2017</h2>  <p class="content__text">- Viewing the report before downloading.</p>
        `
        },
        timesheet_changelog: {
            text: 
        `
        <h2 class="content__title">15 May 2017</h2>      
        <p class="content__text">- Now you can save the selected report parameters as a template.</p> 
        <p class="content__text">- Now you can configure the sending of reports to your mail on a schedule.</p> 
        <p class="content__text">- Fixed the problem with the lack of an Epic link from the subtask.</p> 
        <p class="content__text">- Now you can send a report to your e-mail.</p> 
        <h2 class="content__title">24 March 2017</h2>        
        <p class="content__text">- Optimization of report unloading, which made it possible to generate a report 10 times faster.</p>        
        <h2 class="content__title">20 March 2017</h2>        
        <p class="content__text">- Fixed the problem with displaying an incomplete list of Executors with some rights settings.</p>  
        <h2 class="content__title">17 March 2017</h2>  
        <p class="content__text">- Fixed the problem with incorrect generation of report resolution in Safari.</p>
        <h2 class="content__title">1 March 2017</h2>  
        <p class="content__text">- Published add-on in the production.</p>
        `
        },
        privacy: {
            text: 
        `
        <p class="content__text content__text_main content__text_first-offset">This statement is applying to "Smart Timesheet Report for JIRA" plugin developed by UNIT6 company.</p>
        
        <h2 class="content__title content__title_main">Overview</h2>
        <p class="content__text content__text_main">UNIT6 company works in development since 2006 and have a   great experience in large web projects for different customers.

        The aim of this document is to cover all information about the Data Security & Privacy Statement for "Smart Timesheet Report for JIRA" plugin. Also it describes basic principles that we are using in development and maintenance of this plugin. It should help our customer see what we do and what we really don't with their data.</p>

        <h2 class="content__title content__title_main">Data storage and location</h2>
        <p class="content__text content__text_main">- We use Microsoft Azure platform for hosting our plugin.
        - All data stored in MsSqlServer database.
        - All data storages are encrypted for better security.
        - We store only data for integration with atlassian connect platform: unique ID of atlassian cloud product and access keys. We don't store any information from our customers, that's why we don't violate any law in any country.</p>

        <h2 class="content__title content__title_main">Backups</h2>
        <p class="content__text content__text_main">- Our backups data securely stored in reliable microsoft azure storage.
        - Unauthorized access of backup data is not possible.
        - We backup at least once a day, and we keep the backups at least for a month.
        - We also keep some backups for a year.
        - We provide RTO 8 hours and RPO 24 hours maximum. And we minimized risk of such situation. It should be real huge disruption in 2 azure datacenters simultaneously.
        - We provide RTO 8 hours and RPO 24 hours maximum. And we minimized risk of such situation. It should be real huge disruption in 2 azure datacenters simultaneously.</p>

        <h2 class="content__title content__title_main">Account removal and data retention</h2>
        <p class="content__text content__text_main">- All customer data may be removed any time by uninstalling the AddOn from Atlassian Cloud product.
        - Customer data saved in backups for long period and removed automatically when backup expired, see "backups" item for details.</p>

        <h2 class="content__title content__title_main">Data portability</h2>
        <p class="content__text content__text_main">- We don't support any migrations to other products.
        - We don't have any data for export.</p>

        <h2 class="content__title content__title_main">Application and infrastructure security</h2>
        <p class="content__text content__text_main">- All customer related data stored in encrypted drives.
        - We transfer all data through secured channels.
        - We don't use information from production databases for testing purposes.
        - We don't provide any access to our infrastructure to third party companies.
        - Customers are responsible for maintaining the security of their own Confluence and JIRA Cloud login information.
        - AddOn server is only accessible through secure protocols (e.g. https).
        - All exchange between addon and any atlassian product made with according with atlassian connect platform recommendations.</p>

        <h2 class="content__title content__title_main">Security disclosure</h2>
        <p class="content__text content__text_main">- Security breaches or vulnerabilities with the proposed solution of the problem are published on our documentation.
        - Customers can report security breaches or vulnerabilities using ms@jiratime.com e-mail address.</p>

        <h2 class="content__title content__title_main">Privacy</h2>
        <p class="content__text content__text_main">Data collected during the use of our add-on will not be shared with third parties except if required by law.</p>
        `
        },
        license: {
            text: 
        `
        <h2 class="content__title content__title_main">License</h2>
        <p class="content__text content__text_main">Subject to Your full compliance with all the terms and conditions of this Agreement, "Smart Timesheet Report for JIRA" grants You a nontransferable, nonsublicensable, nonexclusive license, revocable at "Smart Timesheet Report for JIRA"’s discretion, to use the software in object code form only that You will receive through this download (the “Client”), the accompanying documentation, and any additional software that may be made available by "Smart Timesheet Report for JIRA" from time to time for use with the Client (collectively “Software”) for Your use only and only in accordance with the accompanying documentation. Any other use must be pre-approved by "Smart Timesheet Report for JIRA" in writing. This Agreement allows You to run the Software only as received at the time of download, in a single installation of JIRA, for the number of authorized users and nodes.</p>

        <h2 class="content__title content__title_main">Restrictions</h2>
        <p class="content__text content__text_main">You shall not, nor permit anyone else to, directly or indirectly:

        1. copy, modify, or distribute the Software or license key (if any);

        2. reverse engineer, disassemble, decompile or otherwise attempt to discover the source code or structure, sequence and organization of all or any part of the Software (except that this restriction shall not apply to the limited extent restrictions on reverse engineering are prohibited by applicable local law);

        3. rent, lease, or use the Software for timesharing or service bureau purposes, or otherwise use the Software for any commercial purpose.

        You shall maintain and not remove or obscure any proprietary notices on the Software, and shall reproduce such notices exactly on all permitted copies of the Software. As between the parties, "Smart Timesheet Report for JIRA" shall own all title, ownership rights, and intellectual property rights in and to the Software, and any copies or portions thereof. You understand that "Smart Timesheet Report for JIRA" or its licensors may modify or discontinue offering the Software at any time. This Agreement does not give You any rights not expressly and unambiguously granted herein.
        </p>

        <h2 class="content__title content__title_main">Intellectual Property</h2>
        <p class="content__text content__text_main">As a condition to Your use of the Software, You represent, warrant and covenant that You will not use the Software:

        1. to infringe the intellectual property or proprietary rights, or rights of publicity or privacy, of any third party;

        2. to violate any applicable law, statute, ordinance or regulation;

        3. to disseminate, transfer or store information or materials in any form or format ("Content") that are harmful, threatening, abusive, harassing, tortious, defamatory, vulgar, obscene, libelous, or otherwise objectionable or that otherwise violate any law or right of any third party;

        4. to disseminate any software viruses or any other computer code, files or programs that may interrupt, destroy or limit the functionality of any computer software or hardware or telecommunications equipment, or violate the security of any computer network;

        5. to run “bots,” “robots,” any form of auto-responder, or "spam," or any processes that run or are activated while You are not logged in.

        You, not "Smart Timesheet Report for JIRA", remain solely responsible for all Content that You upload, post, e-mail, transmit, or otherwise disseminate using, or in connection with, the Software. You acknowledge that all Content You access through use of the Software is accessed at Your own risk and You will be solely responsible for any damage or liability to any party resulting from such access.</p>

        <h2 class="content__title content__title_main">Support</h2>
        <p class="content__text content__text_main">This Agreement entitles You to email support, upgrades, patches, enhancements, and fixes (collectively, “Support”) for one (1) year following the commencement of a fully paid support term for this version of the Software (or the next version, at "Smart Timesheet Report for JIRA"’s sole discretion, during such one (1) year support term), provided that You comply with all the terms and conditions of this Agreement.</p>

        <h2 class="content__title content__title_main">Warranty Disclaimer</h2>
        <p class="content__text content__text_main">The software is provided “as is”. "Smart Timesheet Report for JIRA" makes no warranty of any kind, express or implied, and "Smart Timesheet Report for JIRA" expressly disclaims all warranties, including without limitation, any implied warranties of merchantability, fitness for a particular purpose and noninfringement. Further, "Smart Timesheet Report for JIRA" does not warrant results of use or that the software is bug free or error free or that its use will be uninterrupted. This disclaimer of warranty constitutes an essential part of this agreement. All the foregoing disclaimers also apply in full with respect to "Smart Timesheet Report for JIRA"’s licensors, suppliers, distributors, contractors and agents.</p>

        <h2 class="content__title content__title_main">Limitation of Remedies and Damages</h2>
        <p class="content__text content__text_main">Under no circumstances and under no legal theory, including, but not limited to, tort, contract, negligence, strict liability, or otherwise, shall "Smart Timesheet Report for JIRA" be liable to you or any other person:

        1. for any indirect, special, incidental, or consequential damages of any character or

        2. for any matter beyond its reasonable control.

        "Smart Timesheet Report for JIRA"’s liability for damages of any kind whatsoever arising out of this agreement shall be limited to the total fees paid by you to "Smart Timesheet Report for JIRA", except where not permitted by applicable law, in which case "Smart Timesheet Report for JIRA"’s liability shall be limited to the minimum amount permitted by such applicable law. All the foregoing limitations shall apply even if "Smart Timesheet Report for JIRA" has been informed of the possibility of such damages. All the foregoing limitations also apply with respect to "Smart Timesheet Report for JIRA"’s suppliers, licensors, distributors, contractors and agents.</p>

        <h2 class="content__title content__title_main">Indemnity</h2>
        <p class="content__text content__text_main">You agree that "Smart Timesheet Report for JIRA" and its licensors, distributors, contractors and agents shall have no liability whatsoever for any use You make of the Software. You shall indemnify and hold harmless "Smart Timesheet Report for JIRA" and its licensors, suppliers, distributors, contractors and agents from any claims, damages, liabilities, costs and fees (including reasonable attorneys’ fees) arising from

        1. Your failure to comply with any term of this Agreement; or

        2. use of the Software in combination with other hardware, software or other systems that would have been avoided but for such use or combination.

        To the maximum extent permitted by applicable law, You hereby release, and waive all claims against, "Smart Timesheet Report for JIRA" and its licensors, suppliers, employees and agents from any and all liability for claims, damages (actual and consequential), costs and expenses (including litigation costs and attorney's fees) of every kind and nature, arising out of or in any way connected with use of the Software.</p>

        <h2 class="content__title content__title_main">Termination</h2>
        <p class="content__text content__text_main">This Agreement shall continue until terminated as set forth in this section. You may terminate this Agreement at any time. Your rights under this Agreement will terminate automatically and irrevocably without notice from "Smart Timesheet Report for JIRA" if You fail to comply with any term(s) of this Agreement, including any attempt to transfer a copy of the Software or Software license key (if any) to another party except as provided in this Agreement. Upon termination for any reason, the Agreement granted hereunder shall terminate and You shall immediately discontinue all use of the Software and destroy and remove from all computers, hard drives, networks and other storage media all copies of the Software, but the terms of this Agreement will otherwise remain in effect.</p>

        <h2 class="content__title content__title_main">Miscellaneous</h2>
        <p class="content__text content__text_main">This Agreement constitutes the entire agreement between you and UNIT6 with respect to the subject matter hereof and will supersede and replace all prior understandings and agreements, in whatever form, regarding the subject matter. We reserve the right to modify this Agreement at any time by providing such revised Agreement to you or by publishing the revised Agreement on the Website. Your continued use of the Software shall constitute your acceptance to be bound by the terms and conditions of the revised Agreement. If any clause of this Agreement is declared invalid, illegal or not enforceable, the clause concerned will be stricken, and the remainder of the Agreement will remain fully in force. Our failure to exercise any right hereunder shall not operate as a waiver of our rights to exercise such right or any other right in the future. We are allowed at our sole discretion to assign this Agreement or any rights hereunder without giving prior notice.</p>
        `
        }
    }
}