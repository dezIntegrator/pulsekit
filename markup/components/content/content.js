(function () {

    var $content = $('.s-content');
    var $back2top = $('.back-to-top');
    var $windowHeight;
    var contentBottom;

    function onScrollWindow() {
        var offsetY = window.pageYOffset;

        if (offsetY - $content.offset().top > 100 && !$back2top.hasClass('back-to-top_visible')) {
            $back2top.addClass('back-to-top_visible');
        } else {
            if (offsetY - $content.offset().top < 100 && $back2top.hasClass('back-to-top_visible')) {
                $back2top.removeClass('back-to-top_visible');
            }
        }

        if (offsetY + $windowHeight >= contentBottom && !$back2top.hasClass('back-to-top_pinned')) {
            $back2top.addClass('back-to-top_pinned');
        } else {
            if (offsetY + $windowHeight < contentBottom && $back2top.hasClass('back-to-top_pinned')) {
                $back2top.removeClass('back-to-top_pinned');
            }
        }

    }

    function onResizeWindow() {
        $windowHeight = $(window).height();
        contentBottom = $content.offset().top + $content.height() + 80;
    }

    $(document).ready(function () {
        if ($content.length < 1 || $back2top.length < 1) {
            return;
        }

        $windowHeight = $(window).height();
        contentBottom = $content.offset().top + $content.height() + 80;

        document.addEventListener('scroll', function () {
            onScrollWindow();
        }, true);

        window.addEventListener('resize', function () {
            onResizeWindow();
        }, true);

    });
})();
