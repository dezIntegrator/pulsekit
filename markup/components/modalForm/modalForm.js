﻿window.modalForm = {
    /**
     * Добавляет обработчик отправки формы в модальных окнах
     * @param {JQuery} $form
     * @param {string} url
     * @param {function=} cbSuccess
     * */
    modalFormAddEvent: function ($form, url, cbSuccess) {

        if (!$form || !url) {
            return;
        }

        cbSuccess = cbSuccess || function () {};

        function modalFormSubmitSuccess($form) {

            var $modal = $form.closest('.b-modal');
            var $thanks = $modal.find('.b-modal__thanks');
            form.toggleThanksEl($form, $thanks);

            setTimeout(function () {
                moduleModal.closeModal($modal);
                form.toggleThanksEl($thanks, $form);
                cbSuccess();
            }, 2000);
        }

        form.formAddEvent($form, url, modalFormSubmitSuccess.bind(this, $form));

    }
};
