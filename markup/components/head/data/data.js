var data = {
    head: {
        main: {
            title: 'Pulse.kit for JIRA cloud',
            source: "http://pulsekit.com/",
            description: 'Set of add-ons Pulse.kit for JIRA cloud',
            sharingLink: "http:pulsekit.com/static/img/content/mainpage/pulsekit_sharing.png"
        },
        timesheet: {
            title: 'Pulse.Timesheet for JIRA cloud',
            source: "http://pulsekit.com/timesheet/",
            description: 'Smart Timesheet plugin for JIRA',
            sharingLink: "http://pulsekit.com/static/img/content/timesheet/timesheet_sharing.png"
        },
        checklist: {
            title: 'Pulse.Checklist for JIRA cloud',
            source: "http://pulsekit.com/checklist/",
            description: 'Checklist plugin for JIRA',
            sharingLink: "http://pulsekit.com/static/img/content/checklist/checklist_sharing.png"
        }
    }
}