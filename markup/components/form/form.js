﻿window.form = {

    /**
     * Закрываем форму, показываем спасибо или наоборот
     * @param {JQuery} $elShow
     * @param {JQuery} $elHide

     * */
    toggleThanksEl: function ($elShow, $elHide) {

        $elShow.css({
            'display': 'none'
        });

        $elHide.css({
            'display': 'block'
        });

    },

    /**
     * Добавляет обработчик отправки формы
     * @param {JQuery} $form
     * @param {string} url
     * @param {function=} cbSuccess
     * */
    formAddEvent: function ($form, url, cbSuccess) {

        if (!$form || !url) {
            return;
        }

        cbSuccess = cbSuccess || function () {};

        var formIsSent = false;

        var URL_SUBMIT_FORM = url;
        // var URL_SUBMIT_FORM = '../sendmail/testSuccess.json'; // for test

        $form.on('submit', function (e) {

            if (formIsSent) {
                return false;
            }
            e.preventDefault();
            formIsSent = true;

            $form.addClass('has-loading');
            $.ajax({
                url: URL_SUBMIT_FORM,
                type: 'POST',
                // type: 'GET', // for test
                data: $(this).serialize(),
                dataType: 'json',
                success: function (json, textStatus) {
                    if (json.error) {
                        for (var key in json.error) {
                            // Если нужно будет обрабатывать ошибки от сервера
                            // $form.addClass('has-error').removeClass('has-success');
                            // $form.find('input[name="' + key + '"]').addClass('has-error');
                            // $form.find('textarea[name="' + key + '"]').addClass('has-error');

                            // $('.b-form__error-text').html(json.error[key]);
                        }
                    }
                    if (json.success) {
                        // Если нужно будет обрабатывать ошибки от сервера
                        // $form.removeClass('has-error').addClass('has-success');
                        // $form.find('.has-error').removeClass('has-error');
                        // $('.b-form__error-text').html( json.success );

                        cbSuccess();
                    }
                },
                complete: function (XMLHttpRequest, textStatus) {
                    // отладочная информация в консоль если письма не ходят или на фронте нет сообщения успех
                    // console.warn(XMLHttpRequest)
                    // console.warn(textStatus)

                    $form.removeClass('has-loading');
                    formIsSent = false;
                }
            });

            return false;
        });

    }

};

