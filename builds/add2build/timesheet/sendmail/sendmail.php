<?php

// Mail Setup
// ==========

// Email content can be modified in the sendmail.php file.

$json = array();

// $email = isset( $_POST['email'] ) ? $_POST['email'] : '';

$comments = isset( $_POST['comments'] ) ? $_POST['comments'] : '';
$email = isset( $_POST['email'] ) ? $_POST['email'] : '';
$subscribe_updates = isset( $_POST['subscribe_updates'] ) ? $_POST['subscribe_updates'] : false;

if(!$email || !preg_match('/^[^\@]+@.*\.[a-z]{2,6}$/i', $email ) ) {
   $json['error']['email'] = 'Please enter your email.';
}

if(!$comments) {
   $json['error']['comments'] = 'Please enter your comments.';
}

// TODO: изменить на два независимых файла. В JS идет два запроса (два промиса, успех когда сработали оба (если есть галка подписаться на рассылку))
if( $subscribe_updates ){

    if ($email) {
    	require '../subscribe/subscribeMailchimp.php';
        $result = subscribeMailchimp($email);
        $json['subscribe'] = $result;
    }
    else{
        $json['error']['subscribe'] = 'Subscription failed.';
    }
}

// If no errors
if( !isset( $json['error'] ) ) {
    // Email text
    $mail_message = "From: site Pulse.Timesheet<br />";

    if($email) {
        $mail_message .= "E-mail: " . $email . "<br />";
    }
    $mail_message .= "Сomments: " . $comments . "<br />";
    $mail_message .= "Subscribe updates: " . ($subscribe_updates ? 'Yes' : 'no') . "<br />";
    // Email title

    require 'PHPMailerAutoload.php';

    $mail = new PHPMailer;


    /////*** это параметры для работы локально! ***////

    $mail->isSMTP();
    $mail->SMTPSecure = 'tls';
    $mail->Host = "smtp.yandex.com";
    $mail->Port = 587;
    $mail->SMTPAuth = true;
    $mail->Username = "blythecatalogue@yandex.ru";
    $mail->Password = "Volkov123";

    /////*** END это параметры для работы локально! ***////

    // здесь указать тот же e-mail что и $mail->Username !
    // второй парамет  - это имя "от Кого" у письма
    $mail->setFrom('blythecatalogue@yandex.ru', $email);

    // здесь указать куда и на чье имя отправить письмо (можно отправлять куда угодно), имя - это "кому" chertopoloh@gmail.com
    $mail->addAddress('info@pulsekit.com', 'PULSE.Timesheet');
    $mail->isHTML(true);

    // Тема письма
    $mail->Subject = 'PULSE.Timesheet. Feedback';

    $mail->Body = $mail_message;

    $mail->AltBody = 'This is a plain-text message body';

    //$mail->addAttachment('images/phpmailer_mini.png');
    if (!$mail->send()) {
        $json['error']['message'] = 'Error while subscribing. Please try again later!';
        error_log ($mail->ErrorInfo);
    } else {
        $json['success'] = 'Thanks for comments!';
    }
}

echo json_encode( $json );

?>
