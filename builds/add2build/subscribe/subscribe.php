<?php
mb_internal_encoding("UTF-8");
$json = array();

$email = isset( $_POST['email'] ) ? $_POST['email'] : '';

if(!$email || !preg_match('/^[^\@]+@.*\.[a-z]{2,6}$/i', $email ) ) {
   $json['error']['email'] = 'Please enter your email.';
}

if ($email) {
    require 'subscribeMailchimp.php';
    $result = subscribeMailchimp($email);

    $json['subscribe'] = $result;

    // TODO: сделать проверку на наличие этого email в базе
    // if ($result.status == 'error' && $result.code == 214) {
    //     $json['success'] = 'Email is already subscribed to the list';
    // } else {
    //     $json['success'] = 'Thanks for subscribe!';
    // }
       
    $json['success'] = 'Thanks for subscribe!';   
}

echo json_encode( $json );

?>