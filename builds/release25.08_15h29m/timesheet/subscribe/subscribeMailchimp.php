<?php 

/**
* @param {string} $email
**/
function subscribeMailchimp($email)
{
	if (!$email) {
		return false;
	}

	require 'class.mailchimp.php';

	// Данные для конкретного аккаунта mailchimp и списка подписки
	$apiKeys = 'a1c64ee16d187d335268c9ee0b5f0fea-us15'; // http://kb.mailchimp.com/integrations/api-integrations/about-api-keys
	$listId = '345b0aeac3'; // http://kb.mailchimp.com/lists/manage-contacts/find-your-list-id

	$mc = new Mailchimp($apiKeys);
    $result = $mc->call("lists/subscribe", array("id" => $listId, "email" => array("email" => $email), "send_welcome" => true ) );

	return $result;
}

?>